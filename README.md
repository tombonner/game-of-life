# game-of-life
Tom's Conway's Game of Life

* This is an HTML5 Canvas, CSS, JavaScript version of Conway's Game of Life.
* A live demonstration of this code can be viewed at http://tbonner.atwebpages.com/gol/gol.html
